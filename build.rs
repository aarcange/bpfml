use libbpf_cargo::SkeletonBuilder;
use std::env;
use std::path::PathBuf;

fn build(name: &str, clang_args: &str) {
    let mut out =
        PathBuf::from(env::var_os("OUT_DIR")
		      .expect("OUT_DIR must be set in build script"));
    out.push(format!("{}.skel.rs", name));
    SkeletonBuilder::new()
        .source(format!("src/bpf/{}.bpf.c", name))
	.clang_args(format!("-mcpu=v3 -Werror {}", clang_args))
	.debug(true)
        .build_and_generate(&out)
        .unwrap();
}

fn main() {
    build("ml_prefault_0_d", "-DDEBUG");
    build("ml_prefault_1_d", "-DDEBUG");
    if !cfg!(debug_assertions) {
	build("ml_prefault_0", "");
	build("ml_prefault_1", "");
	build("ml_prefault_0_s", "-DSIMULATION");
	build("ml_prefault_1_s", "-DSIMULATION");
    }
    println!("cargo:rerun-if-changed=src/bpf");
}
