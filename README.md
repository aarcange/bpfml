# BPF Machine Learning PoC

This is an experimental Proof of Concept of Machine Learning models
implemented with eBPF that are deployed in real time while workloads
run.

The ML prefault models are trained to infer the addresses of future
page faults and so they are able to resolve not one, but multiple page
faults at once. This way the overall number of short lived page faults
that can't be immediately mapped with Transparent Hugepages can be
reduced.

The ML model to deploy needs to be selected with the --prefault
parameter.

ml_prefault model 0 will learn faults forward and backwards within a
certain distance, it learns the fault history up to PAST_NR faults
before the current fault and it uses 32bits per fault prediction.

ml_prefault model 1 can only predict forward faults at the next
virtual address, it doesn't remember more than 1 step backwards and it
uses 1 bit per fault prediction.

Without the [patch](patch) applied BPFML will only run in
simulation mode with the --simulation argument.

With the patch applied the effect of the inferred page faults can be
monitored with bpftrace:

```
bpftrace -e ' kfunc:__handle_mm_fault { if (@pre[tid] == 0) { @fault[comm] = count(); @zfault = count() } @faulttot[comm] = count(); @zfaulttot = count(); @pre[tid] = 1 } kretfunc:handle_mm_fault { delete(@pre[tid]) } END { clear(@pre); } '
```

## memhog - no BPFML:

```
perf stat -e minor-faults,major-faults -r3 numactl -C 0 -m 0 memhog -H -r1 12G >/dev/null

 Performance counter stats for 'numactl -C 0 -m 0 memhog -H -r1 12G' (3 runs):

         9,437,594      minor-faults:u                                                          ( +- 19.25% )
                 3      major-faults:u                                         

            3.5759 +- 0.0124 seconds time elapsed  ( +-  0.35% )
```

## memhog - BPFML model 0:

### 1st run:

```
perf stat -e minor-faults,major-faults -r3 numactl -C 0 -m 0 memhog -H -r1 12G >/dev/null

 Performance counter stats for 'numactl -C 0 -m 0 memhog -H -r1 12G' (3 runs):

         3,170,693      minor-faults:u                                                          ( +-  0.23% )
                 0      major-faults:u                                         

             2.728 +- 0.525 seconds time elapsed  ( +- 19.23% )
```

### 2nd run:

```
perf stat -e minor-faults,major-faults -r3 numactl -C 0 -m 0 memhog -H -r1 12G >/dev/null

 Performance counter stats for 'numactl -C 0 -m 0 memhog -H -r1 12G' (3 runs):

            37,230      minor-faults:u                                                          ( +- 19.24% )
                 0      major-faults:u                                         

           2.20635 +- 0.00687 seconds time elapsed  ( +-  0.31% )
```

## memhog - BPFML model 1:

### 1st run:

```
perf stat -e minor-faults,major-faults -r3 numactl -C 0 -m 0 memhog -H -r1 12G >/dev/null

 Performance counter stats for 'numactl -C 0 -m 0 memhog -H -r1 12G' (3 runs):

         3,170,719      minor-faults:u                                                          ( +-  0.23% )
                 0      major-faults:u                                         

             2.717 +- 0.521 seconds time elapsed  ( +- 19.17% )
```

### 2nd run:

```
perf stat -e minor-faults,major-faults -r3 numactl -C 0 -m 0 memhog -H -r1 12G >/dev/null

 Performance counter stats for 'numactl -C 0 -m 0 memhog -H -r1 12G' (3 runs):

            37,275      minor-faults:u                                                          ( +- 19.24% )
                 0      major-faults:u                                         

           2.20034 +- 0.00861 seconds time elapsed  ( +-  0.39% )
```

## python timeit - BPFML off:

```
perf stat -e minor-faults,major-faults -r3 numactl -C 0 -m 0 python -m timeit "for i in range(400000): x = 'x'*i"
1 loop, best of 5: 1.59 sec per loop
1 loop, best of 5: 1.59 sec per loop
1 loop, best of 5: 1.58 sec per loop

 Performance counter stats for 'numactl -C 0 -m 0 python -m timeit for i in range(400000): x = 'x'*i' (3 runs):

        10,942,448      minor-faults                                                            ( +- 19.25% )
                 0      major-faults                                           

           10.5617 +- 0.0221 seconds time elapsed  ( +-  0.21% )
```

## python timeit - BPFML model 0:

```
perf stat -e minor-faults,major-faults -r3 numactl -C 0 -m 0 python -m timeit "for i in range(400000): x = 'x'*i"
1 loop, best of 5: 1.48 sec per loop
1 loop, best of 5: 1.48 sec per loop
1 loop, best of 5: 1.48 sec per loop

 Performance counter stats for 'numactl -C 0 -m 0 python -m timeit for i in range(400000): x = 'x'*i' (3 runs):

           284,192      minor-faults                                                            ( +- 17.56% )
                 0      major-faults                                           

            9.5507 +- 0.0134 seconds time elapsed  ( +-  0.14% )
```

## python timeit - BPFML model 1:

```
perf stat -e minor-faults,major-faults -r3 numactl -C 0 -m 0 python -m timeit "for i in range(400000): x = 'x'*i"
1 loop, best of 5: 1.48 sec per loop
1 loop, best of 5: 1.48 sec per loop
1 loop, best of 5: 1.48 sec per loop

 Performance counter stats for 'numactl -C 0 -m 0 python -m timeit for i in range(400000): x = 'x'*i' (3 runs):

           707,946      minor-faults                                                            ( +- 18.61% )
                66      major-faults                                           

            9.5752 +- 0.0112 seconds time elapsed  ( +-  0.12% )
```

Quickstart on Fedora:

```
git clone https://gitlab.com/aarcange/bpfml.git && cd bpfml && sudo dnf install cargo libbpf-devel clang hwloc-devel && cargo build --release && sudo ./target/release/bpfml -p 0 --simulation
```

# Internals and Design

## ML models

### NoNN

The current models in BPFML are of the NoNN (No Neural Network)
kind, built from scratch to maximize training and inference speed,
especially to reduce the amount of CPU L1 cachelines used by the
model.

There's no need of a full fledged neural network in order to recognize
and predict events that are worth learning only if they repeat
identically every time, down to the last computer bit.

Only the "model 0" and "model 1" have been released in this PoC, but
more aggressive models that can cross vmas and/or run in rust feeded
by libbpf-async ringbufs also have been tested. The two models of the
PoC provide for the simplest possible implementation to facilitate
their evaluation.

### Training and inference

Training and inference are always running in real time.

Throttling "training" sounds appealing, but it provided mixed results so
it has been dropped by the PoC. It is an area that might have
potential.

A possibly simple alternative is to concentrate the model where it
pays off the most, for example in a qemu build that would be the cc1
process:

```
        struct task_struct *current;
        current = (struct task_struct *)bpf_get_current_task();
        int comm2 = 0;
        if (BPF_CORE_READ_INTO(&comm2, current, comm))
                return 0;
        char *comm = (char *)&comm2;
        if (comm[0] != 'c' || comm[1] != 'c' || comm[2] != '1')
                return 0;
```

The above is however a far from optimal way to focalize the model and
such information should be provided with a flag in the
bpf_prefault_data structure instead.

While "training" and "inference" are always on, they don't necessarily run
at the same time and they are usually interleaved.

When "training" figures that it already knew the present, it activates
"inference". So then the model stops "training" and switches to predict
the future only, until it doesn't know the future anymore or something
interrupts the "inference" sequence and "training" runs again.

## ml_prefault target

### Objective

The ml_prefault target objective is to resolve N page faults instead
of 1 for every hardware page fault, by relying on the future
prediction of the N-1 faults that would happen next coming from one of
the BPFML models.

The net effect is similar to reducing the number of syscalls by a
factor of N, but it also allows to reuse the CPU cache and locks.

The ml_prefault is currently concentrated on the virtual areas not
backed by THP/hugetlbfs because that's where the low hanging fruit
is. THP already drops the page faults by an order of 512 so while
prefaulting THP is possible too, it only provids diminishing returns
that would hardly offset the non zero cost of running the model.

The ml_prefault models will consume memory (visible in "bpftool map
show"). This is a fixed RAM cost and it should be tuned at runtime in
function of the RAM of the NUMA node (in the current PoC it is tunable
at build time only).

### OOT patch

There's room for further optimizations in the bpf_prefault OOT patch
that can't be leveraged in the PoC to reduce the risk of future
rejects. For example do_fault_around with ->map_pages is more optimal
than bpf_prefault at taking the PT lock only once, which is also why
the fault_around_bytes sysctl hasn't been disabled while a bpf program
is attached to the bpf_prefault hook.

### Side channel

With the ml_prefault target enabled, one app might try to measure the
order of the page faults of other apps, that in theory might create a
side channel. [Similar side channels existed on the
pagecache](https://arxiv.org/pdf/1901.01161.pdf), but there's no
mincore() syscall equivalent to probe the ml_prefault.

While it's currently unclear if this might be a concern, if this would
become a concern it would be possible to perturb the inference maps
based on the current memcg. Alternatively it will be possible to move
the ml_inference maps in the per cgroup local storage, but ideally
that should be done with the primary objective of focalizing the model
on a single container, so different models could then run on different
containers.

## Next target

The next target would be to apply the "model 0" on seeking synchronous
read I/O, that can't take advantage of readahead to keep the nvme
queue full even without requiring userland to use asyncio.
