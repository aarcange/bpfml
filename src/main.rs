// SPDX-License-Identifier: GPL-2.0-or-later OR AGPL-3.0-or-later
/*
 *  Copyright (C) 2023  Red Hat, Inc.
 */

extern crate hwloc2;

use libbpf_rs::skel::{SkelBuilder,OpenSkel,Skel};
use libbpf_rs::{MapType,MapHandle,MapFlags};
use libbpf_rs::{libbpf_sys};
use std::os::fd::{AsFd,AsRawFd};
use std::mem::size_of;
use clap::Parser;
use hwloc2::{Topology, ObjectType};
use std::thread::sleep;
use std::time::Duration;
#[cfg(not(debug_assertions))]
use std::fs::File;
#[cfg(not(debug_assertions))]
use std::io::{Read,Write};

macro_rules! ebpf_module {
    ($module:ident, $file:expr) => {
	mod $module {
	    include!(concat!(env!("OUT_DIR"), $file));
	}
	use $module::*;
    }
}

ebpf_module!(ml_prefault_0_d, "/ml_prefault_0_d.skel.rs");
ebpf_module!(ml_prefault_1_d, "/ml_prefault_1_d.skel.rs");

#[cfg(not(debug_assertions))]
ebpf_module!(ml_prefault_0, "/ml_prefault_0.skel.rs");
#[cfg(not(debug_assertions))]
ebpf_module!(ml_prefault_1, "/ml_prefault_1.skel.rs");
#[cfg(not(debug_assertions))]
ebpf_module!(ml_prefault_0_p, "/ml_prefault_0_s.skel.rs");
#[cfg(not(debug_assertions))]
ebpf_module!(ml_prefault_1_p, "/ml_prefault_1_s.skel.rs");

#[derive(Parser)]
#[command(version, about, author)]
#[command(help_template="\
{before-help}{name} {version}
{about-with-newline}{author-with-newline}{usage-heading} {usage}

{all-args}{after-help}
")]
pub struct Config {
    #[arg(short, long,
	  value_parser=clap::value_parser!(u8).range(0..2))]
    prefault_model: u8,

#[cfg(not(debug_assertions))]
    #[arg(short, long)]
    debug: bool,

#[cfg(not(debug_assertions))]
    #[arg(short, long)]
    simulation: bool,
}

fn setup_maps<'a,B>(bpf_build: B)
		    -> Result<<<B as SkelBuilder<'a>>::Output
			       as OpenSkel>::Output,
			      Box<dyn std::error::Error>> where B: SkelBuilder<'a> {
    let mut bpf_open = bpf_build.open()?;

    let topo = Topology::new().unwrap();
    let cpus = topo.objects_with_type(&ObjectType::PU).unwrap();
    let nr_cpus = cpus.iter().count().try_into()?;
    let mut nodes = cpus.iter().map(
	|cpu| { cpu.nodeset().unwrap().first() })
	.collect::<Vec<i32>>();
    nodes.dedup();
    let nodes = nodes;

    let ml_pasts = bpf_open.open_object_mut().map_mut("ml_pasts").unwrap();
    ml_pasts.set_max_entries(nr_cpus)?;
    let ml_inferences = bpf_open.open_object_mut().map_mut("ml_inferences").unwrap();
    ml_inferences.set_max_entries(nodes.len().try_into()?)?;

    let bpf_load = bpf_open.load()?;

    let ml_past = bpf_load.object().map("ml_past").unwrap();
    let ml_past_info = ml_past.info().unwrap().info;
    let ml_pasts = bpf_load.object().map("ml_pasts").unwrap();
    let ml_pasts_info = ml_pasts.info().unwrap().info;

    let ml_inference = bpf_load.object().map("ml_inference").unwrap();
    let ml_inference_info = ml_inference.info().unwrap().info;
    let ml_inferences = bpf_load.object().map("ml_inferences").unwrap();
    let ml_inferences_info = ml_inferences.info().unwrap().info;

    let btf_fd = unsafe { libbpf_sys::bpf_btf_get_fd_by_id(ml_past_info.btf_id) };
    let mut opts = libbpf_sys::bpf_map_create_opts {
	map_flags: libbpf_sys::BPF_F_NUMA_NODE,
	sz: size_of::<libbpf_sys::bpf_map_create_opts>() as libbpf_sys::size_t,
	btf_fd: btf_fd as u32,
	btf_key_type_id: ml_past_info.btf_key_type_id,
	btf_value_type_id: ml_past_info.btf_value_type_id,
	btf_vmlinux_value_type_id: ml_past_info.btf_vmlinux_value_type_id,
	..Default::default()
    };
    let mut consumed = false;
    for cpu in cpus {
	let cpuset = cpu.cpuset().unwrap();
	let nodeset = cpu.nodeset().unwrap();
	assert_eq!(cpuset.weight(), 1);
	assert_eq!(nodeset.weight(), 1);
	let cpu = cpuset.first();
	let node = nodeset.first();
	assert!(cpu >= 0);
	assert!(node >= 0);
	assert!(nodes.contains(&node));
	assert!(cpu < ml_pasts_info.max_entries.try_into()?);
	println!("Create map for cpu {:3} on node {:3}", cpu, node);
	let map_cpu = if node == 0 && !consumed {
	    consumed = true;
	    MapHandle::try_clone(ml_past)
	} else {
	    opts.numa_node = node as u32;
	    MapHandle::create(
		MapType::Array,
		Some(format!("ml_past{}", cpu)),
		ml_past_info.key_size,
		ml_past_info.value_size,
		ml_past_info.max_entries,
		&opts,
	    )
	}.unwrap();
	ml_pasts.update((cpu as u32).to_ne_bytes().as_slice(),
			(map_cpu.as_fd().as_raw_fd() as u32)
			.to_ne_bytes().as_slice(),
			MapFlags::ANY).unwrap();
    }
    let mut opts = libbpf_sys::bpf_map_create_opts {
	map_flags: libbpf_sys::BPF_F_NUMA_NODE,
	sz: size_of::<libbpf_sys::bpf_map_create_opts>() as libbpf_sys::size_t,
	btf_fd: btf_fd as u32,
	btf_key_type_id: ml_inference_info.btf_key_type_id,
	btf_value_type_id: ml_inference_info.btf_value_type_id,
	btf_vmlinux_value_type_id: ml_inference_info.btf_vmlinux_value_type_id,
	..Default::default()
    };
    consumed = false;
    for node in nodes {
	assert!(node < ml_inferences_info.max_entries.try_into()?);
	println!("Create inference map for node {:4}", node);
	let map_cpu = if node == 0 && !consumed {
	    consumed = true;
	    MapHandle::try_clone(ml_inference)
	} else {
	    opts.numa_node = node as u32;
	    MapHandle::create(
		MapType::Array,
		Some(format!("ml_inference{}", node)),
		ml_inference_info.key_size,
		ml_inference_info.value_size,
		ml_inference_info.max_entries,
		&opts,
	    )
	}.unwrap();
	ml_inferences.update((node as u32).to_ne_bytes().as_slice(),
			     (map_cpu.as_fd().as_raw_fd() as u32)
			     .to_ne_bytes().as_slice(),
			     MapFlags::ANY).unwrap();
    }
    Ok(bpf_load)
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let config = Config::parse();

#[cfg(not(debug_assertions))]
    let mut bpf_load : Box<dyn Skel> = match config.prefault_model {
	0 => if config.debug {
	    Box::new(setup_maps(MlPrefault0DSkelBuilder::default())?)
	} else if config.simulation {
	    Box::new(setup_maps(MlPrefault0SSkelBuilder::default())?)
	} else {
	    Box::new(setup_maps(MlPrefault0SkelBuilder::default())?)
	},
	1 => if config.debug {
	    Box::new(setup_maps(MlPrefault1DSkelBuilder::default())?)
	} else if config.simulation {
	    Box::new(setup_maps(MlPrefault1SSkelBuilder::default())?)
	} else {
	    Box::new(setup_maps(MlPrefault1SkelBuilder::default())?)
	},
	_ => panic!("unknown model"),
    };

#[cfg(debug_assertions)]
    let mut bpf_load : Box<dyn Skel> = match config.prefault_model {
	0 => Box::new(setup_maps(MlPrefault0DSkelBuilder::default())?),
	1 => Box::new(setup_maps(MlPrefault1DSkelBuilder::default())?),
	_ => panic!("unknown model"),
    };

    bpf_load.attach()?;
    println!("BPFML prefault model {} started", config.prefault_model);

#[cfg(not(debug_assertions))]
    if config.simulation {
	let trace_pipe = "/sys/kernel/debug/tracing/trace_pipe";
	println!("cat {}", trace_pipe);
	let mut file = File::open(trace_pipe)?;
	let mut buf = vec![0; 128*1024];
	loop {
            let len = file.read(&mut buf)?;
            if len == 0 {
		return Ok(())
	    }
	    std::io::stdout().write_all(&buf[..len])?;
	}
    }

    loop {
	sleep(Duration::from_secs(1<<31));
    }
}
