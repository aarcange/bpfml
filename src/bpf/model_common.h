// SPDX-License-Identifier: GPL-2.0-or-later OR AGPL-3.0-or-later
/*
 *  Copyright (C) 2023  Red Hat, Inc.
 */

#ifdef SIMULATION
#define bpf_prefault_data __bpf_prefault_data
struct bpf_prefault_data {
	long unsigned int pfd_addr;
	short unsigned int pfd_nr;
	short unsigned int pfd_nr_pages;
	short unsigned int pfd_flags;
	bool pfd_write;
	struct mm_struct *pfd_mm;
	struct vm_area_struct *pfd_vma;
	long unsigned int pfd_vm_start;
	long unsigned int pfd_vm_end;
	long unsigned int pfd_vm_flags;
	long unsigned int pfd_mm_code_size;
};
#endif

struct ml_past {
	__uint(type, BPF_MAP_TYPE_ARRAY);
	__uint(map_flags, BPF_F_NUMA_NODE);
	__uint(numa_node, 0);
	__type(key, u32);
	__type(value, struct past);
	__uint(max_entries, 1);
} ml_past SEC(".maps");

struct ml_inference {
	__uint(type, BPF_MAP_TYPE_ARRAY);
	__uint(map_flags, BPF_F_NUMA_NODE);
	__uint(numa_node, 0);
	__type(key, u32);
	__type(value, struct inference);
	__uint(max_entries, INFERENCE_NR);
} ml_inference SEC(".maps");

struct {
	__uint(type, BPF_MAP_TYPE_ARRAY_OF_MAPS);
	__uint(map_flags, BPF_F_NUMA_NODE);
	__uint(numa_node, 0);
	__type(key, u32);
	__type(value, u32);
	__uint(max_entries, 0);
	__array(values, struct ml_past);
} ml_pasts SEC(".maps");

struct {
	__uint(type, BPF_MAP_TYPE_ARRAY_OF_MAPS);
	__uint(map_flags, BPF_F_NUMA_NODE);
	__uint(numa_node, 0);
	__type(key, u32);
	__type(value, u32);
	__uint(max_entries, 0);
	__array(values, struct ml_inference);
} ml_inferences SEC(".maps");

static __always_inline struct past *get_past(void)
{
	u32 map_idx = bpf_get_smp_processor_id();
	void *map = bpf_map_lookup_elem(&ml_pasts, &map_idx);
	if (likely(map)) {
		map_idx = 0;
		map = bpf_map_lookup_elem(map, &map_idx);
	}
	return map;
}

static __always_inline void *get_inference(void)
{
	u32 node = bpf_get_numa_node_id();
	return bpf_map_lookup_elem(&ml_inferences, &node);
}
