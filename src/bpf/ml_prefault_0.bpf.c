// SPDX-License-Identifier: GPL-2.0-or-later OR AGPL-3.0-or-later
/*
 *  Copyright (C) 2023  Red Hat, Inc.
 */

#include "vmlinux.h"
#include <bpf/bpf_core_read.h>
#include <bpf/bpf_helpers.h>

//#define SIMULATION
//#define DEBUG
#define BPF_PREFAULT_NR 255

#define PAGE_BITS 12 // FIXME
#define PAGE_SIZE (1UL<<PAGE_BITS)

#define CACHELINE_SIZE 64

#define SLOT_BITS 20
#define SLOT_NR (1ULL<<SLOT_BITS)
#define SLOT_MASK (SLOT_NR-1)

#define INFERENCE_BITS (26-SLOT_BITS)
#define INFERENCE_NR (1ULL<<INFERENCE_BITS)
#define INFERENCE_MASK (INFERENCE_NR-1)

#define SLOW_KEY_NR 512

#define OFFSET_VAL_BITS 9
#define OFFSET_VAL_BIT (32-OFFSET_VAL_BITS)
#define OFFSET_WRITE_BIT (OFFSET_VAL_BIT-1)
#define OFFSET_KEY_BITS (OFFSET_WRITE_BIT)

#define OFFSET_WRITE (1<<OFFSET_WRITE_BIT)

#define PAST_NR 4
#define FAST_KEY_SHIFT_BITS ((OFFSET_KEY_BITS+PAST_NR-1)/PAST_NR)

#define OFFSET_KEY_MASK ((1<<OFFSET_KEY_BITS)-1)
//#define SIZE_MIN (512<<PAGE_BITS)

#if defined(DEBUG) || defined(SIMULATION)
#define DBG(fmt, ...) bpf_printk(fmt, ## __VA_ARGS__)
#else
#define DBG(fmt, ...)
#endif

#define likely(x) __builtin_expect(!!(x), 1)
#define unlikely(x) __builtin_expect(!!(x), 0)

extern void fail_build(void);

#include "murmur3.h"
static __always_inline u64 bitmix(u64 v) {
	return fmix64(v);
}

struct past {
	u32 slow_key;
	u32 fast_key;
	u32 mm;
	char match;
	bool can_train;
} __attribute__((__aligned__(CACHELINE_SIZE)));

struct inference {
	u32 offset[SLOT_NR];
};

#include "model_common.h"

static __always_inline u32 slow_key_to_inference_idx(u32 key)
{
	return key & INFERENCE_MASK;
}

static __always_inline u32 *get_slot(void *map, u32 key, u32 oldkey,
				     struct inference **oldinference)
{
	struct inference *inference = *oldinference;
	if (inference && (oldkey & ~SLOT_MASK) == (key & ~SLOT_MASK))
		return &inference->offset[key & SLOT_MASK];
	u32 map_idx = slow_key_to_inference_idx(key >> SLOT_BITS);
	inference = bpf_map_lookup_elem(map, &map_idx);
	/* FIXME */
	if (unlikely(inference)) {
		*oldinference = inference;
		return &inference->offset[key & SLOT_MASK];
	}
	return NULL;
}

static __always_inline u32 fast_key_to_offset_key(u32 key)
{
	return key & OFFSET_KEY_MASK;
}

static __always_inline bool good_key(u32 offset_key, u32 offset)
{
       return (offset & OFFSET_KEY_MASK) == offset_key;
}

static __always_inline u32 read_once(u32 *src) {
	return *(volatile u32 *)src;
}

static __always_inline void write_once(u32 *dst, u32 src) {
	*(volatile u32 *)dst = src;
	//__sync_lock_test_and_set(dst, src);
}

static __always_inline unsigned long do_inference(bool *write,
						  unsigned long *old_offset,
						  void *map,
						  struct inference **inference,
						  u32 oldkey,
						  struct past *past,
						  u32 fast_key,
						  u32 slow_key,
						  unsigned long start,
						  unsigned long end,
						  bool linear)
{
	u32 *inf = get_slot(map, slow_key, oldkey, inference);
	if (unlikely(!inf)) {
		past->can_train = false;
		return 0;
	}

	u32 ioffset = read_once(inf);
	s32 delta = (s32) ioffset >> OFFSET_VAL_BIT;
	if (linear && delta != -1)
		return 0;
	if (!ioffset)
		return 0;
	if (!good_key(fast_key_to_offset_key(fast_key),
		      ioffset)) {
		return 0;
	}
	unsigned long addr = *old_offset;
	addr -= delta;
	unsigned long new_offset = addr;
	addr <<= PAGE_BITS;
	addr += start;
	if (unlikely(addr < start || addr >= end)) {
		DBG("vma collision %lx %lx %lx", start, addr, end);
		goto err;
	}

	*write = !!(ioffset & OFFSET_WRITE);

	past->can_train = false;
	*old_offset = new_offset;
#ifndef SIMULATION
	return addr;
#else
	DBG("prefault %016lx", addr);
	return 0;
#endif

err:
	write_once(inf, 0);
	past->can_train = false;
	return 0;
}

static __always_inline void do_train(void *map,
				     struct inference **inference,
				     u32 oldkey,
				     struct past *past,
				     u32 fast_key,
				     u32 slow_key,
				     u32 offset,
				     bool write)
{
	u32 *inf = get_slot(map, slow_key, oldkey, inference);
	if (unlikely(!inf))
		return;

	u32 ioffset = read_once(inf);
	u32 offset_key = fast_key_to_offset_key(fast_key);
	offset |= offset_key;
	offset |= write ? OFFSET_WRITE : 0;
	if (!ioffset) {
		write_once(inf, offset);
	} else if (ioffset != offset) {
		if (!good_key(offset_key, ioffset) ||
		    (offset & ~OFFSET_WRITE) == ioffset) {
			write_once(inf, offset);
		} else {
			write_once(inf, 0);
		}
	} else
		past->can_train = false;
}

static __always_inline u32 build_slow_key(u64 subkey, unsigned long offset)
{
	return subkey + offset;
}

static __always_inline u32 build_fast_key(u64 subkey, unsigned long offset,
					  bool write, u32 fast_key)
{
	u32 new_fast_key = bitmix(subkey ^
				  (u64) offset ^
				  ((u64) write << 32));
	new_fast_key ^= fast_key << FAST_KEY_SHIFT_BITS;
	return new_fast_key;
}

static __always_inline
unsigned long fault_prediction(struct bpf_prefault_data *pfd,
			       struct past *past)
{
	unsigned long ret = 0;

	unsigned long start = pfd->pfd_vm_start;
	unsigned long end = pfd->pfd_vm_end;
	unsigned long size = (end - start) >> PAGE_BITS;

#ifdef SIZE_MIN
	if (size < (SIZE_MIN >> PAGE_BITS))
		goto out;
#endif

	u64 subkey = size;
	subkey ^= (u64) pfd->pfd_mm_code_size << 32;
	subkey ^= (u64) pfd->pfd_vm_flags;
	subkey = bitmix(subkey);

	unsigned long addr = pfd->pfd_addr;
	unsigned long offset = (addr - start) >> PAGE_BITS;

	bool infer_mode = !!(pfd->pfd_flags & BPF_PREFAULT_FLAG_INFER_MODE);

	u32 fast_key, new_fast_key, slow_key, new_slow_key;
	fast_key = new_fast_key = past->fast_key;
	slow_key = new_slow_key = past->slow_key;
	if (!infer_mode) {
		new_slow_key = build_slow_key(subkey, offset);
		past->slow_key = new_slow_key;

		new_fast_key = build_fast_key(subkey, offset, pfd->pfd_write,
					      fast_key);
		past->fast_key = new_fast_key;
	}

	struct mm_struct *mm = pfd->pfd_mm;
	if (unlikely(past->mm != (u32) (u64) mm)) {
		past->mm = (u32) (u64) mm;
		past->match = 0;
		past->can_train = false;
	}

	if (past->match < PAST_NR) {
		past->match++;
		goto out;
	}

	void *map = get_inference();
	if (unlikely(!map))
		goto out;
	struct inference *inference = NULL;

	if (infer_mode)
		past->can_train = false;

	if (past->can_train) {
		s32 diff = slow_key - new_slow_key;
		s32 diff_small = diff << OFFSET_VAL_BIT;

		if (diff_small >> OFFSET_VAL_BIT == diff &&
		    slow_key != new_slow_key) {
			do_train(map, &inference, slow_key,
				 past, fast_key, slow_key,
				 diff_small,
				 pfd->pfd_write);
		} else
			past->can_train = false;
	}

	if (past->can_train)
		goto out;

	bool write;
	past->can_train = true;
	for (unsigned int idx = 0; idx < BPF_PREFAULT_NR; idx++) {
		addr = do_inference(&write, &offset,
				    map, &inference, slow_key,
				    past, new_fast_key, new_slow_key,
				    start, end, !!idx);
		if (!addr)
			break;
		if (!idx) {
			pfd->pfd_write = write;
			ret = addr;
		} else if (write != pfd->pfd_write)
			break;

		pfd->pfd_nr_pages = idx + 1;

		slow_key = new_slow_key;
		new_slow_key = build_slow_key(subkey, offset);
		past->slow_key = new_slow_key;

		fast_key = new_fast_key;
		new_fast_key = build_fast_key(subkey, offset, write,
					      fast_key);
		past->fast_key = new_fast_key;
	}
out:
	return ret;
}

static __always_inline void bpf_prefault(struct bpf_prefault_data *pfd)
{
	if (SLOW_KEY_NR != (1<<OFFSET_VAL_BITS))
		fail_build();

	struct past* past = get_past();
	/* FIXME */
	if (likely(!past)) {
		DBG("%s %i missing past", __FILE__, __LINE__);
		return;
	}
	unsigned long addr = fault_prediction(pfd, past);
	if (addr) {
		pfd->pfd_addr = addr;
		pfd->pfd_nr = BPF_PREFAULT_NR;
	}
}

#include "model_common.c"
