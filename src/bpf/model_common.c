// SPDX-License-Identifier: GPL-2.0-or-later OR AGPL-3.0-or-later
/*
 *  Copyright (C) 2023  Red Hat, Inc.
 */

#ifdef SIMULATION
//#include <bpf/bpf_tracing.h>
SEC("fentry/handle_mm_fault")
int fentry__handle_mm_fault(unsigned long long *ctx) {
	struct vm_area_struct *vma = (struct vm_area_struct *)ctx[0];
	unsigned long addr = (unsigned long)ctx[1];
	unsigned int flags = (unsigned int)ctx[2];

	struct bpf_prefault_data pfd;
	pfd.pfd_addr = addr;
	pfd.pfd_nr = 0;
	pfd.pfd_nr_pages = 0;
	pfd.pfd_flags = 0;
	pfd.pfd_write = !!(flags & FAULT_FLAG_WRITE);
	struct mm_struct *mm = vma->vm_mm;
	pfd.pfd_mm = mm;
	pfd.pfd_vma = vma;
	pfd.pfd_vm_start = vma->vm_start;
	pfd.pfd_vm_end = vma->vm_end;
	pfd.pfd_vm_flags = vma->vm_flags;
	pfd.pfd_mm_code_size = mm->end_code - mm->start_code;
	bpf_prefault(&pfd);
	return 0;
}
#else
struct bpf_prefault_data_ctx {
	struct bpf_prefault_data *pfd;
	unsigned int version;
};
SEC("raw_tp.w/bpf_prefault")
int raw_tracepoint__bpf_prefault(struct bpf_prefault_data_ctx *ctx) {
	if (likely(ctx->version == 0))
		bpf_prefault(ctx->pfd);
	return 0;
}
#endif

char LICENSE[] SEC("license") = "GPL";
