// SPDX-License-Identifier: GPL-2.0-or-later OR AGPL-3.0-or-later
/*
 *  Copyright (C) 2023  Red Hat, Inc.
 */

#include "vmlinux.h"
#include <bpf/bpf_core_read.h>
#include <bpf/bpf_helpers.h>

//#define SIMULATION
//#define DEBUG
#define BPF_PREFAULT_NR 255

#define PAGE_BITS 12 // FIXME
#define PAGE_SIZE (1UL<<PAGE_BITS)

#define CACHELINE_SIZE 64

#define BITMAP_BITS 5
#define BITMAP_NR (1<<BITMAP_BITS)
#define BITMAP_MASK (BITMAP_NR-1)

#define SLOT_BITS 19
#define SLOT_NR (1ULL<<SLOT_BITS)
#define SLOT_MASK (SLOT_NR-1)

#define INFERENCE_BITS (26-SLOT_BITS-BITMAP_BITS)
#define INFERENCE_NR (1ULL<<INFERENCE_BITS)
#define INFERENCE_MASK (INFERENCE_NR-1)

#define OFFSET_KEY_BITS 31
#define OFFSET_KEY_MASK ((1<<OFFSET_KEY_BITS)-1)

//#define SIZE_MIN (512<<PAGE_BITS)

#if defined(DEBUG) || defined(SIMULATION)
#define DBG(fmt, ...) bpf_printk(fmt, ## __VA_ARGS__)
#else
#define DBG(fmt, ...)
#endif

#define likely(x) __builtin_expect(!!(x), 1)
#define unlikely(x) __builtin_expect(!!(x), 0)

extern void fail_build(void);

#include "murmur3.h"
static __always_inline u64 bitmix(u64 v) {
	return fmix64(v);
}

struct past {
	u32 offset;
	u32 vma;
	bool can_train;
} __attribute__((__aligned__(CACHELINE_SIZE)));

struct slot {
	u32 write:1;
	u32 key:OFFSET_KEY_BITS;
	u32 bitmap;
};

union slotu {
	struct slot slot;
	u64 update;
};

struct inference {
	union slotu slot[SLOT_NR];
};

#include "model_common.h"

static __always_inline u32 slow_key_to_inference_idx(u32 key)
{
	return key & INFERENCE_MASK;
}

static __always_inline union slotu *get_slot(void *map, u64 key, u64 oldkey,
					     struct inference **oldinference)
{
	struct inference *inference = *oldinference;
	key >>= BITMAP_BITS;
	oldkey >>= BITMAP_BITS;
	if (inference && (oldkey & ~SLOT_MASK) == (key & ~SLOT_MASK))
		return &inference->slot[key & SLOT_MASK];
	u32 map_idx = slow_key_to_inference_idx(key >> SLOT_BITS);
	inference = bpf_map_lookup_elem(map, &map_idx);
	/* FIXME */
	if (unlikely(inference)) {
		*oldinference = inference;
		return &inference->slot[key & SLOT_MASK];
	}
	return NULL;
}

static __always_inline void set_bit(struct slot *slot, u64 key)
{
	u32 mask = 1 << (key & BITMAP_MASK);
	slot->bitmap |= mask;
}

static __always_inline void clear_bit(struct slot *slot, u64 key)
{
	u32 mask = 1 << (key & BITMAP_MASK);
	slot->bitmap &= ~mask;
}

static __always_inline bool test_bit(struct slot *slot, u64 key)
{
	u32 mask = 1 << (key & BITMAP_MASK);
	return !!(slot->bitmap & mask);
}

static __always_inline void read_once(union slotu *dst, union slotu *src) {
	dst->update = *(volatile u64 *)&src->update;
}

static __always_inline void write_once(union slotu *dst, union slotu *src) {
	*(volatile u64 *) &dst->update = src->update;
	//__sync_lock_test_and_set(&dst->update, src->update);
}

static __always_inline unsigned long do_inference(bool *write,
						  unsigned short *nr_pages,
						  void *map,
						  struct inference **inference,
						  struct past *past,
						  u32 fast_key,
						  u64 new_slow_key,
						  u64 slow_key,
						  unsigned long addr,
						  unsigned long start,
						  unsigned long end)
{
	struct slot newslot;
	union slotu slotu, *slot;
	unsigned long curr, found;
	unsigned int i = 0;

	slot = get_slot(map, new_slow_key, slow_key, inference);
	if (unlikely(!slot))
		return 0;
	read_once(&slotu, slot);

	curr = new_slow_key & BITMAP_MASK;
	for (found = curr; found < BITMAP_NR; found++) {
		if (!(slotu.slot.bitmap & (1 << found)))
			break;
	}
	if (unlikely(found == curr))
		return 0;
	newslot.key = fast_key;
	/* FIXME */
	if (likely(newslot.key != slotu.slot.key))
		return 0;
	addr += PAGE_SIZE;

	if (unlikely(addr < start || addr >= end)) {
		DBG("vma collision %lx %lx %lx", start, addr, end);
		goto err;
	}
	found -= curr;
	*nr_pages = found;
	unsigned long found_end;
	found_end = (addr & ~(PAGE_SIZE-1)) + (found << PAGE_BITS);
	if (unlikely(found_end > end)) {
		*nr_pages -= (found_end - end) >> PAGE_BITS;
		DBG("vma collision end %lx %lx", found_end, end);
	}
	*write = !!slotu.slot.write;

	past->can_train = false;
#ifndef SIMULATION
	return addr;
#else
	DBG("prefault %016lx-%016lx", addr, addr + (*nr_pages << PAGE_BITS));
	return 0;
#endif

err:
	clear_bit(&slotu.slot, new_slow_key);
	write_once(slot, &slotu);
	past->can_train = false;
	return 0;
}

static __always_inline bool do_train(void *map,
				     struct inference **inference,
				     struct past *past,
				     u32 fast_key,
				     u64 slow_key,
				     bool write)
{
	union slotu *slot = get_slot(map, slow_key, 0, inference);
	if (unlikely(!slot))
		return true;

	union slotu slotu;
	read_once(&slotu, slot);

	if (!slotu.slot.bitmap) {
		slotu.slot.write = write;
		slotu.slot.key = fast_key;
		set_bit(&slotu.slot, slow_key);
		write_once(slot, &slotu);
		return true;
	}

	struct slot newslot;
	newslot.key = fast_key;
	if (slotu.slot.key != newslot.key || !slotu.slot.write && write) {
		slotu.slot.write = write;
		slotu.slot.key = fast_key;
		slotu.slot.bitmap = 0;
		set_bit(&slotu.slot, slow_key);
		write_once(slot, &slotu);
		return true;
	}
	if (!test_bit(&slotu.slot, slow_key)) {
		set_bit(&slotu.slot, slow_key);
		write_once(slot, &slotu);
		return true;
	}
	return false;
}

static __always_inline
unsigned long fault_prediction(struct bpf_prefault_data *pfd,
			       struct past *past)
{
	unsigned long ret = 0;

	unsigned long addr = pfd->pfd_addr;
	struct vm_area_struct *vma = pfd->pfd_vma;
	unsigned long start = pfd->pfd_vm_start;
	unsigned long offset = (addr - start) >> PAGE_BITS;
	unsigned long old_offset = offset - 1;

	if (unlikely(past->offset != old_offset ||
		     past->vma != (u32) (u64) vma))
		goto out;

	unsigned long end = pfd->pfd_vm_end;
	unsigned long size = (end - start) >> PAGE_BITS;

#ifdef SIZE_MIN
	if (size < (SIZE_MIN >> PAGE_BITS))
		goto out;
#endif

	u64 subkey = size;
	subkey ^= (u64) pfd->pfd_mm_code_size << 32;
	subkey ^= (u64) pfd->pfd_vm_flags;
	subkey = bitmix(subkey);

	u64 new_slow_key = subkey + offset;
	u64 slow_key = new_slow_key - 1;
	u32 fast_key = subkey >> (INFERENCE_BITS+SLOT_BITS+BITMAP_BITS);

	void *map = get_inference();
	if (unlikely(!map))
		goto out;
	struct inference *inference = NULL;

	if (pfd->pfd_flags & BPF_PREFAULT_FLAG_INFER_MODE)
		past->can_train = false;
	if (past->can_train)
		past->can_train = do_train(map, &inference, past,
					   fast_key ^
					   (slow_key >> BITMAP_BITS),
					   slow_key, pfd->pfd_write);
	if (past->can_train)
		goto out;

	bool write;
	past->can_train = true;
	pfd->pfd_nr_pages = 0;
	for (unsigned int idx = 0; idx < BPF_PREFAULT_NR; idx++) {
		unsigned short nr_pages;
		addr = do_inference(&write, &nr_pages,
				    map, &inference, past,
				    fast_key ^ (new_slow_key >> BITMAP_BITS),
				    new_slow_key, slow_key,
				    addr, start, end);
		if (!addr)
			break;
		/* FIXME */
		if (unlikely(!idx)) {
			pfd->pfd_write = write;
			ret = addr;
		} else if (write != pfd->pfd_write)
			break;

		pfd->pfd_nr_pages += nr_pages;
		slow_key = new_slow_key;
		new_slow_key += nr_pages;
		offset += nr_pages;

		if (unlikely(pfd->pfd_nr_pages >= BPF_PREFAULT_NR)) {
			pfd->pfd_nr_pages = BPF_PREFAULT_NR;
			break;
		}
	}
out:
	past->offset = offset;
	past->vma = (u32) (u64) vma;
	return ret;
}

static __always_inline void bpf_prefault(struct bpf_prefault_data *pfd)
{
	if (64-(INFERENCE_BITS+SLOT_BITS+BITMAP_BITS) < OFFSET_KEY_BITS)
		fail_build();

	struct past* past = get_past();
	/* FIXME */
	if (likely(!past)) {
		DBG("%s %i missing past", __FILE__, __LINE__);
		return;
	}

	unsigned long addr = fault_prediction(pfd, past);
	if (addr) {
		pfd->pfd_addr = addr;
		pfd->pfd_nr = BPF_PREFAULT_NR;
	}
}

#include "model_common.c"
